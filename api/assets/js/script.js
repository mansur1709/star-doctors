$(document).ready(function() {
  $("#myForm").validate({
    rules: {
      name: {
        required: true,
        minlength: 2
      },
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        minlength: 10,
        maxlength: 10
      },
      subject: {
        required: true,
        minlength: 3
      }
    },
    messages: {
      name: {
        required: "Please enter your name",
        minlength: "Name should be at least 2 characters long"
      },
      email: {
        required: "Please enter your email address",
        email: "Please enter a valid email address"
      },
      phone: {
        required: "Please enter your phone number",
        minlength: "Phone number should be at least 10 digits long",
        maxlength :"Only 10 Dight Numbers Alloweded"
      },
      subject: {
        required: "Please enter a subject",
        minlength: "Subject should be at least 3 characters long"
      }
    },
    submitHandler: function(form) {
      var formData = $(form).serialize();

      $("#overlay").fadeIn(300);　
     
      // Log the form data to the console
      console.log(formData);
          $.ajax({
            type: 'POST',
            url:'mail.php',
            data: formData,
            success:function(response){
              $('#success').text('Email sent succesfully',response)
            }
          }).fail(function(response){
            alert("POST request failed");
          }).done(function() {
            setTimeout(function(){
              $("#overlay").fadeOut(300);
            },500);
          });;
        }
  });
});



/*

$(document).ready(function() {
  $("#myForm").validate({
    rules: {
      name: {
        required: true,
        minlength: 2
      },
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        minlength: 10
      },
      subject: {
        required: true,
        minlength: 3
      }
    },
    messages: {
      name: {
        required: "Please enter your name",
        minlength: "Name should be at least 2 characters long"
      },
      email: {
        required: "Please enter your email address",
        email: "Please enter a valid email address"
      },
      phone: {
        required: "Please enter your phone number",
        minlength: "Phone number should be at least 10 digits long"
      },
      subject: {
        required: "Please enter a subject",
        minlength: "Subject should be at least 3 characters long"
      }
    },
    submitHandler: function(form) {
      var formData = $(form).serialize();
      
      // Log the form data to the console
      console.log(formData);
      $.ajax({
        url:'../../mail.php',
        type: 'post',
        data: formData,
        success:function(response){
          $('#success').text('Form Sumbitted Succesfully',response)
        }
      }).fail(function(){
        alert("POST request failed");
      });

    }
  });
});


 /*     $('#submit-button').click(function(event){
      event.preventDefault(); 
       console.log('Name: ' + $('#name-input').val());
      console.log('Phone: ' + $('#phone-input').val());
      console.log('Email: ' + $('#email-input').val());
      console.log('Subject: ' + $('#subject-input').val()); 
      console.log('form :'+$('#myForm'))
       $.ajax({
        url:'',
        type: 'post',
        data: $('#myForm').serialize(),
        success:function(response){
          $('#success').text('Form Sumbitted Succesfully',response)
        }
      }).fail(function(){
        alert("POST request failed");
      })
     }) 
 */