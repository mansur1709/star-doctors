<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Star Doctors - Education Consultancy</title>
    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css2?family=Jost:wght@300;400;600;700&display=swap" rel="stylesheet">
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="icon" type="image/x-icon" href="assets/images/favicon-16x16.png">
</head>

<body>
    
    
    <header id="site-header" class="fixed-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark stroke">
                <h1>
                    <a class="navbar-brand" href="index.php">
                        <img src="assets/images/logo.png" alt="">
                    </a>
                </h1>
                <div class="navbar-collapse collapse show" id="navbarTogglerDemo02">
                    <div class="ml-lg-auto contact-us">
                        <a href="#subscribe" class="btn btn-style btn-primary">Contact Us</a>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!--/header-->
   
     <section class="bannerw3l-hnyv">
        <div class="banner-layer">
            <div id="demo" class="carousel slide m-0" data-ride="carousel">
                <ul class="carousel-indicators">
                  <li data-target="#demo" data-slide-to="0" class="active"></li>
                  <li data-target="#demo" data-slide-to="1"></li>
                
                </ul>
                
                
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="assets/images/banner-1.jpg" alt="1" class="cimg" >
                  </div>
                  <div class="carousel-item">
                    <img src="assets/images/banner-2.jpg" alt="2" class="cimg"  >
                  </div>
                
                </div>
                
                
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                  <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                  <span class="carousel-control-next-icon"></span>
                </a>
              </div>



        </div>
    </section>
    
    <section class="w3l-homeblock1" id="about">
        <div class="midd-w3 py-5">
            <div class="container py-lg-5 py-md-3">
                <div class="row cwp23-grids align-items-center">
                    <div class="col-lg-6">
                        <h5 class="title-subw3hny">About</h5>
                        <h3 class="title-w3l">Star Doctors</h3>
                     
                        <p>Star doctors educational consultancy is in Mannargudi, in the state of Tamil Nadu in India. We provide consultancy in the education sector. We cater to the needs of students who wish to become doctors by providing information about the opportunities available in foreign countries to study medical courses, helping them in getting admission to college and taking care of their needs till they complete their studies.</p>
                    </div>
                    <div class="HomeAboutImages col-lg-6 mt-lg-0 mt-5 pl-lg-5">
                        <div class="cwp23-text-cols">
                            <img src="assets/images/doctors.jpg" alt="" class="doctors">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="w3l-stats-main" id="stats">
        <div class="container">
            <div class="row stats-con py-lg-0 py-5">
                <div class="col-lg-6 stats-content-right mb-5 mt-5">
                    <a href="#service" class="d-block zoom"><img src="assets/images/ab1.jpg" alt=""
                            class="img-fluid" /></a>
                </div>
                <div class="col-lg-6 stats-content-left pl-lg-5">

                    <div class="text-left mt-lg-5 mt-4">
                       
                        <div class="stat-icon-des">

                          
                            
                            <p class="para-stat">
                            When a student wishes or dreams of becoming a doctor, he or she has to go through a series of hard work and motivated studies. It starts right from the planning & preparation which should also be directed towards the target and has to start very early. Doctor’s as an individual serves a greater purpose in life to society and their own family. No doubt it is a very prestigious profession with handsome money but to opt for this it requires some rigorous efforts to do so. Some steps are to be followed to becoming a doctor & proper guidance must require. Star Doctors help you to achieve your dream from all ends.
                            </p>
                            <br>
                           
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </section>
    
    <section class="w3l-testimonials" id="testimonials">
        
        <div class="cusrtomer-layout py-5">
            <div class="container py-lg-4 py-md-3 py-2 pb-lg-0">
                <h3 class="title-w3l two text-center mb-sm-5 mb-4">
                    <span class="inn-text">Colleges</span>
                </h3>
                <div class="testimonial-width">
                    <div id="owl-demo1" class="owl-two owl-carousel owl-theme owl-loaded owl-drag">
                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2777px;">
                                <div class="owl-item" style="width: 376.667px; margin-right: 20px;">
                                    <div class="item">
                                        <div class="testimonial-content">
                                            <div class="testimonial">
                                                <img src="assets/images/college-4.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 376.667px; margin-right: 20px;">
                                    <div class="item">
                                        <div class="testimonial-content">
                                            <div class="testimonial">
                                            <img src="assets/images/college-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 376.667px; margin-right: 20px;">
                                    <div class="item">
                                        <div class="testimonial-content">
                                            <div class="testimonial">
                                                <img src="assets/images/college-3.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 376.667px; margin-right: 20px;">
                                    <div class="item">
                                        <div class="testimonial-content">
                                            <div class="testimonial">
                                            <img src="assets/images/college-1.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                        
                            
                            </div>
                        </div>

                        <!-- <div class="owl-nav disabled">
                            <button type="button" role="presentation" class="owl-prev">
                                <span aria-label="Previous">
                                    <span class="fa fa-angle-left"></span>
                                </span>
                            </button>
                            <button type="button" role="presentation" class="owl-next">
                                <span aria-label="Next">
                                    <span class="fa fa-angle-right"></span>
                                </span>
                            </button>
                        </div> -->
                        <div class="owl-dots">
                            <button role="button" class="owl-dot">
                                <span></span>
                            </button>
                            <button role="button" class="owl-dot">
                                <span></span>
                            </button>
                            <!-- <button role="button" class="owl-dot">
                                <span></span>
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <section class="w3l-project" id="subscribe">
        <div class="container-fluid mx-lg-0">
            <div class="row">
                <div class="col-lg-6 bottom-info">
                    <div class="project-header-section text-left">
                        <h3 class="title-w3l"><span
                            class="inn-text">Contact US</span></h3>
                        <p class="mt-3 pr-lg-5">Star Doctors Educational Consultancy,<br>88, Mela Raja Veethi, Mannargudi 614001</p>
                        
                        <br>

                        <a href="tel:+91 9629283513">+91 9629283513</a>&nbsp;&nbsp;
                        <a href="tel:+79 889979126">+79 889979126</a>
                    
                     
                    <div class="subscribe mt-5">
                        <form  id="myForm" method="POST">
                            <input type="text" id="name-input" name="name" class="form-control" placeholder="Name">
                            <span class="error-message"></span>
                            <br>
                            <input type="number" id="phone-input" name="phone" class="form-control" placeholder=" Mobile Number" >
                            <span class="error-message"></span>
                            <br>
                            <input type="text" id="email-input" name="email" class="form-control" placeholder="Your Email" >
                            <span class="error-message"></span>
                            <br>
                            <input type="text" id="subject-input" name="subject" class="form-control" placeholder="Subject" >
                            <span class="error-message"></span>

                            
                            <button  type="submit"  id="submit-button" class="btn btn-style btn-primary">Subscribe</button>
                            <div id="success" class="mb-3"></div>
                        </form>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 subcribe-img">

                </div>
            </div>
        </div>

        <div id="overlay">
            <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>

    </section>
    <!--//subscribe-->
    <!--/footer-->
    <footer class="w3l-footer-22 position-relative">
        <div class="copyright-footer text-center">
            <div class="container">
                <div class="columns">
                    <p>© 2023 Star doctors. All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    </footer>
    


    <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fas fa-level-up-alt" aria-hidden="true"></span>
    </button>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
   
    <script src="assets/js/jquery-3.6.js"></script>
    <script src="assets/js/theme-change.js"></script>
    
    <script>
        $(function () {
            $('.navbar-toggler').click(function () {
                $('body').toggleClass('noscroll');
            })
        });
    </script>
    <!-- disable body scroll which navbar is in active -->

    <!--/MENU-JS-->
    <script>
        $(window).on("scroll", function () {
            var scroll = $(window).scrollTop();

            if (scroll >= 80) {
                $("#site-header").addClass("nav-fixed");
            } else {
                $("#site-header").removeClass("nav-fixed");
            }
        });

        //Main navigation Active Class Add Remove
        $(".navbar-toggler").on("click", function () {
            $("header").toggleClass("active");
        });
        $(document).on("ready", function () {
            if ($(window).width() > 991) {
                $("header").removeClass("active");
            }
            $(window).on("resize", function () {
                if ($(window).width() > 991) {
                    $("header").removeClass("active");
                }
            });
        });

        
    </script>
    <!--//MENU-JS-->

    <script src="assets/js/owl.carousel.js"></script>
 <!--   <script src="assets/js/validate_jquery.js"></script>
    <script src="assets/js/validate_jquery1.js"></script>  -->

    <script>
        $(document).ready(function () {
            $("#owl-demo1").owlCarousel({
                loop: true,
                margin: 20,
                nav: false,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    768: {
                        items: 2,
                        nav: false
                    },
                    1000: {
                        items: 3,
                        nav: false,
                        loop: false
                    }
                }
            })
        })
    </script>
    <!-- //script for tesimonials carousel slider -->
    <script src="assets/js/bootstrap.min.js"></script>
   

   <script  type="text/javascript"  src="assets/js/script.js"></script>
<!--   <script src="assets/js/validate_jquery.js"></script>
   <script src="assets/js/validate_jquery1.js"></script>  -->
   <script type="text/javascript"  src="assets/js/jquery.validate.min.js">
     </script>
</body>

</html>