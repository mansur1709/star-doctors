<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'include/PHPMailer/src/Exception.php';

require 'include/PHPMailer/src/PHPMailer.php';
require 'include/PHPMailer/src/SMTP.php';

class customException extends Exception
{
    public function errorMessage()
    {
        //error message
        $errorMsg = 'Error on line ' . $this->getLine() . ' in ' . $this->getFile() . ': <b>' . $this->getMessage() . '</b> is not a valid E-Mail address';
        return $errorMsg;
    }
}

$email = $_POST['email'];

try {
    //check if
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        //throw exception if email is not valid
        throw new customException($email);
    }

	$name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $sub = $_POST['subject'];

    $mail = new PHPMailer(true);

    //$mail->SMTPDebug=3;
    $mail->isSMTP();

    $mail->Host ='smtp.gmail.com';
    $mail->SMTPAuth =true;
    
    $mail->Username ='jinnaoff321@gmail.com';  //your email here
    $mail->Password ='nvpjitagslbldtry'; //password

    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;

    $mail->setFrom($email, $name); //your email here
    $mail->addAddress('qmakerssolutions@gmail.com'); 

    $mail->isHTML(true);

    $mail->Subject = $sub;
    $mail->Body = 'Name: ' . $name . "<br /> Mobile Number: " . $phone . "<br /> Email: " . $email;

	$mail->send();
} catch (customException $e) {
    //display custom message
    echo $e->errorMessage();
}
